import React from 'react';
import {StyleSheet, Text, Image, View, TouchableOpacity, FlatList, ScrollView} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import Skill from './components/skillItem'
import data from './skillData.json'


export default class App extends React.Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('../Tugas13/images/logo.png')}/>
                </View>

                <View style={{paddingHorizontal : 15}}>
                    <View style={styles.hello}>
                        <Icon name= "person" size={50} style={{color: '#B4E9FF'}}/>
                        <View style={{marginTop: 8}}>
                            <Text style={styles.font}>Hai,</Text>
                            <Text style={{fontSize: 18,color: '#003366'}}>Anjar Harimurti</Text>
                        </View>
                    </View>

                    <View style={{marginBottom: 10}}>
                        <Text style={{fontSize: 32, color: '#003366'}}>SKILL</Text>
                        <View style={{height: 2, backgroundColor: '#3EC6FF'}}/>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                        <View style={styles.jumbotron}>
                            <Text style={styles.categoryName}>Library / Framework</Text>
                        </View>
                        <View style={styles.jumbotron}>
                            <Text style={styles.categoryName}>Bahasa Pemrograman</Text>
                        </View>
                        <View style={styles.jumbotron}>
                            <Text style={styles.categoryName}>Teknologi</Text>
                        </View>
                    </View>


                    <FlatList
                        data={data.items}
                        renderItem={(skill) => <Skill skill={skill.item}/>}
                        keyExtractor={(item) => item.id}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        alignItems: 'flex-end',
        marginBottom: 50,
        marginTop: 10,
    },
    hello: {
        flexDirection: 'row',
        marginBottom: 10,
    },
    jumbotron: {
        backgroundColor: '#B4E9FF',
        borderRadius: 15,
        padding: 10,
        marginVertical: 10,
    },
    categoryName: {
        fontSize: 14,
        color: '#003366',
        fontWeight: 'bold',
    },

});
