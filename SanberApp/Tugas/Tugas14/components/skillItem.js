import React from 'react';
import {StyleSheet, Text, Image, View, TouchableOpacity, FlatList} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';


export default class App extends React.Component {
    render() {
        let skill = this.props.skill;
        return (

            <View style={styles.jumbotron}>
                <View style={{flexDirection: 'row'}}>
                    <Icon style={styles.icon} name={skill.iconName} size={80} color={'#013162'}/>
                    <View style={{marginLeft: 25}}>
                        <Text style={styles.skillName}>{skill.skillName}</Text>
                        <Text style={styles.skillCategory}>{skill.categoryName}</Text>
                        <Text style={styles.skillPercentage}>{skill.percentageProgress}</Text>
                    </View>

                </View>
                <Icon name= "chevron-right" size={100} style={{color: '#013162'}}/>
            </View>


        )
    }
}

const styles = StyleSheet.create({
    jumbotron: {
        backgroundColor: '#B4E9FF',
        borderRadius: 15,
        padding: 10,
        marginVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    skillName: {
        color: '#003366',
        fontSize: 24,
        fontWeight: 'bold'
    },
    skillCategory: {
        color: '#3EC6FF',
        fontSize: 16,
        textAlign: 'left'
    },
    skillPercentage: {
        color: '#FFFFFF',
        fontSize: 48,
        textAlign: 'left',
    },
});
