import React from 'react';
import { StyleSheet, Text, Image, View, TouchableOpacity, FlatList } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import YoutubeVideo from './components/VideoItem';
import data from './data.json';
import VideoItem from "./components/VideoItem";

export default class App extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={{height: 25, backgroundColor: '#E6E6E6'}}/>
                <View style={styles.navBar}>
                    <Image source={require('./images/logo.png')} style={{width: 98, height: 22}}/>
                    <View style={styles.rightNav}>
                        <TouchableOpacity>
                            <Icon style={styles.navItem} name="search" size={25}/>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <Icon style={styles.navItem} name="account-circle" size={25}/>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.body}>
                    <FlatList
                        data={data.items}
                        renderItem={ (video)=><VideoItem video={video.item}/> }
                        keyExtractor={(item) => item.id}
                        ItemSeparatorComponent={()=><View style={{height:0.5, backgroundColor: '#E5E5E5'}}/>}
                    />
                </View>

                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon style={styles.tabsItem} name="home" size={25}/>
                        <Text style={styles.tabTitle}>Home</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItem}>
                        <Icon style={styles.tabsItem} name="whatshot" size={25}/>
                        <Text style={styles.tabTitle}>Trending</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItem}>
                        <Icon style={styles.tabsItem} name="subscriptions" size={25}/>
                        <Text style={styles.tabTitle}>Subscriptions</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItem}>
                        <Icon style={styles.tabsItem} name="folder" size={25}/>
                        <Text style={styles.tabTitle}>Library</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    navBar: {
        height: 55,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    rightNav: {
        flexDirection: 'row',
    },
    navItem: {
        marginLeft: 25,
        color: '#555555'
    },
    body: {
        flex: 1,
    },
    tabBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around',
        color: '#888888',
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabTitle: {
        fontSize: 11,
        color: '#3C3C3C',
        paddingTop: 4,
    },
    tabsItem: {
        color: '#555555'
    }
});