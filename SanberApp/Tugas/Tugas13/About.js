import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, Image, View, TouchableOpacity, ScrollView, TextInput, Button, Alert } from 'react-native';


import Icon from 'react-native-vector-icons/MaterialIcons';

export default class About extends React.Component {
    render(){
        //alert(data.kind)
        return (
            <ScrollView style={styles.container}>
                <Text style={styles.title}>Tentang saya</Text>

                <View style={{alignItems: 'center'}}>
                    <Icon name="person" size={150}/>
                    <Text style={styles.name}>Anjar Harimurti</Text>
                    <Text style={styles.subTitle}>React Native Developer</Text>
                </View>

                <View style={{padding: 10,marginVertical: 10}}>
                    <View style={styles.jumbotron}>
                        <Text style={styles.heading}>Portofolio</Text>
                        <View style={{height:0.75, backgroundColor:'#363636', marginVertical: 5, marginBottom: 15}}/>
                        <View style={{flexDirection: 'row',justifyContent: 'space-around',}}>
                            <View style={{alignItems: 'center'}}>
                                <Image style={{width: 54, height:50}}  source={require('./images/gitlab.png')}/>
                                <Text>@args06</Text>
                            </View>
                            <View style={{alignItems: 'center'}}>
                                <Image style={styles.image} source={require('./images/github.png')}/>
                                <Text>@args06</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.jumbotron}>
                        <Text style={styles.heading}>Hubungi Saya</Text>
                        <View style={{height:0.75, backgroundColor:'#363636', marginVertical: 5, marginBottom: 15}}/>
                        <View style={{flexDirection: 'column',justifyContent: 'space-around',}}>
                            <View style={styles.socialMedia}>
                                <Image style={styles.image} source={require('./images/facebook.png')}/>
                                <Text style={styles.font}>Anjar Harimurti</Text>
                            </View>
                            <View style={styles.socialMedia}>
                                <Image style={styles.image} source={require('./images/instagram.png')}/>
                                <Text style={styles.font}>@args06</Text>
                            </View>
                            <View style={styles.socialMedia}>
                                <Image style={styles.image} source={require('./images/twitter.png')}/>
                                <Text style={styles.font}>@args06</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    title: {
        fontSize: 36,
        height: 60,
        color: '#003366',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 50,
    },
    name: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold'
    },
    subTitle: {
        fontSize: 16,
        color: '#3EC6FF',
        textAlign: 'center',
        marginTop: 5
    },
    jumbotron: {
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        padding: 10,
        marginVertical: 10
    },
    heading: {
        fontSize: 16,
        color: '#363636',
    },
    font: {
        fontSize: 16,
        color: '#363636',
        fontWeight: 'bold',
        margin: 5,
        textAlignVertical:'center'
    },
    image: {
        width: 50,
        height: 50,
    },
    socialMedia:{
        flexDirection: 'row', marginBottom: 20, marginTop:10
    }
})
