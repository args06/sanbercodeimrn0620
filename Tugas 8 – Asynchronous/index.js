// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var times = 10000

readBooks(times,books[0],function(time) {
	readBooks(times,books[1],function(time) {
		readBooks(times,books[2],function(time) {
			console.log("Selesai baca buku")
		})
	})
})
