//Looping While
console.log("No.1 Looping While");
console.log("\nLOOPING PERTAMA");
var i = 2;
while(i <= 20){
	console.log(i + " - I love coding");
	i+=2;
}
console.log("LOOPING KEDUA");
var i = 20;
while(i >= 2){
	console.log(i + " - I will become a mobile developer");
	i-=2;
}

//Looping For
console.log("\nNo.2 Looping For\n");
for (var i = 1; i <= 20; i++) {
	if (i%2 == 1 && i%3 == 0) {
		console.log(i + " - I Love Coding");
	} else if (i%2 == 0) {
		console.log(i + " - Berkualitas");
	} else if(i%2 == 1){
		console.log(i + " - Santai");
	}
}

//Looping Persegi Panjang
console.log("\nNo.3 Looping Persegi Panjang\n");
for (var i = 0; i < 4; i++) {
	for (var j = 0; j <8 ; j++) {
		process.stdout.write("#");
	}
	console.log("")
}

//Looping Tangga
console.log("\nNo.4 Looping Tangga\n");
for (var i = 0; i < 7; i++) {
	for (var j = 0; j < i+1; j++) {
		process.stdout.write("#");
	}
	console.log("")
}

//Looping Papan Catur
console.log("\nNo.5 Looping Papan Catur\n");
for (var i = 1; i <= 8; i++) {
	if (i%2 == 1) {
		for (var j = 1; j <= 8; j++) {
			if (j%2 == 0) {
				process.stdout.write("#");
			} else {
				process.stdout.write(" ");
			}
		}
	} else if (i%2 == 0){
		for (var j = 1; j <= 8; j++) {
			if (j%2 == 1) {
				process.stdout.write("#");
			} else {
				process.stdout.write(" ");
			}
		}
	}
	console.log();
}