/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini
  constructor(subject,points,email) {
    this.name = name
    this.legs = legs
    this.cold_blooded = cold_blooded
  }

  average(points){
    var length = this.points.length;
    var total;
    if (length > 1) {
      let average;
      for (var i = 0; i < length - 1; i++) {
        total += points[i];
      }
      average = total / length;
      return average;
    } else if(length == 1){
      return points;
    }
  }

  get subject() {
    return this.subject;
  }

  set subject(x) {
    this.subject = x;
  }

  get points() {
    return this.points;
  }

  set points(x) {
    this.points = x;
  }

  get email() {
    return this.email;
  }

  set email(x) {
    this.email = x;
  }

}

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  // code kamu di sini
  var hasil = [];

  for (var i = 1; i < data.length-1; i++) {
    let tampung = {
      email: data[i][0],
      subject: subject,
      points: data[i][parseInt(subject[subject.length-1])]
    }
    hasil.push(tampung);
  }
  //return hasil;
  console.log(hasil);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  // code kamu di sini
  data[0].push("average");
  data[0].push("predikat");

  for (var i = 1; i < data.length; i++) {
    let avg = 0; 
    for (var j = 1; j < 4; j++) {
      avg += data[i][j];
    }
    avg /= 3;
    data[i].push(avg);
    if (avg > 90) {
      data[i].push("honour");
    } else if(avg > 80){
      data[i].push("graduate");
    } else{
      data[i].push("participant");
    }
  }
  console.log("");
  for (var i = 1; i < data.length; i++) {
    console.log(`${i}. Email: ${data[i][0]}`);
    console.log(`Rata-rata : ${data[i][4].toFixed(1)}`);
    console.log(`Predikat : ${data[i][5]}\n`)
  }
}

recapScores(data);