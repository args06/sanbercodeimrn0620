//No.1
console.log("Soal No. 1 (Range)");
function range(startNum, finishNum) {
	var array = [];
	if (startNum == null || finishNum == null) {
		return -1;
	} else if (startNum < finishNum) {
		for (var i = startNum; i <= finishNum; i++) {
			array.push(i);
		}
	} else if (startNum > finishNum) {
		for (var i = startNum; i >= finishNum; i--) {
			array.push(i);
		}
	} else if (startNum == finishNum){
		array.push(startNum);
	}
	return array;
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range());

//No.2
console.log("\nSoal No. 2 (Range with Step)");
function rangeWithStep(startNum, finishNum,step = 1) {
	var array = [];
	if (startNum == null || finishNum == null) {
		return -1;
	} else if (startNum < finishNum) {
		for (var i = startNum; i <= finishNum; i+=step) {
			array.push(i);
		}
	} else if (startNum > finishNum) {
		for (var i = startNum; i >= finishNum; i-=step) {
			array.push(i);
		}
	} else if (startNum == finishNum){
		array.push(startNum);
	}
	return array;
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

//No.3
console.log("\nSoal No. 3 (Sum of Range)");
function sum(startNum, finishNum,step = 1) {
	if (startNum == null && finishNum == null) {
		return 0;
	} else if (finishNum == null) {
		finishNum = startNum;
	}
	var array = rangeWithStep(startNum,finishNum,step);
	var sum = 0;
	for (var i = 0; i < array.length; i++) {
		sum = sum + array[i];
	}
	return sum;
}

console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

//No.4
console.log("\nSoal No. 4 (Array Multidimensi)");
function dataHandling(input) {
    for (var i = 0; i < 4; i++) {
    	console.log("Nomor ID : " + input[i][0]);
    	console.log("Nama Lengkap : " + input[i][1]);
    	console.log("TTL : " + input[i][2] + " " + input[i][3]);
    	console.log("Hobi : " + input[i][4] + "\n");
    }
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ];

dataHandling(input);

//No.5
console.log("\nSoal No. 5 (Balik Kata)");
function balikKata(word) {
    var reversed = "";
    for (var i = word.length - 1; i >= 0; i--) {
        reversed = reversed + word[i];
    }
    return reversed;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

//No.5
console.log("\nSoal No. 6 (Metode Array)");
function monthDetection(month) {
	switch(month){
		case "01":{console.log("Januari"); break;}
		case "02":{console.log("Februari"); break;}
		case "03":{console.log("Maret"); break;}
		case "04":{console.log("April"); break;}
		case "05":{console.log("Mei"); break;}
		case "06":{console.log("Juni"); break;}
		case "07":{console.log("Juli"); break;}
		case "08":{console.log("Agustus"); break;}
		case "09":{console.log("September"); break;}
		case "10":{console.log("Oktober"); break;}
		case "11":{console.log("November"); break;}
		case "12":{console.log("Desember"); break;}
	}
}
function dataHandling2(input) {
	var data = input;
	input.splice(1,2,input[1] + "Elsharawy", "Provinsi " + input[2]);
	input.splice(4,1);
	input.splice(4,0,"Pria", "SMA Internasional Metro");

	var ttlPisah = input[3].split("/");
	console.log(input);

	monthDetection(ttlPisah[1]);
	
	var ttlSort = input[3].split("/");
	console.log(ttlSort.sort(function (value1, value2) { return value2-value1 } ));

	console.log(ttlPisah.join("-"));
	input.splice(1,1,"Roman Alamsyah", "Provinsi " + input[2]);
	console.log(input[1]);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);