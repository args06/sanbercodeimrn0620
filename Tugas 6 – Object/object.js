//SOAL 1
console.log("Soal No. 1 (Array to Object)");
var thisYear = new Date().getFullYear();
function arrayToObject(arr) {
    // Code di sini 
    var object = [];
    var tampung;

    if (arr[0] == null) {
        console.log("Array Kosong");
    } else {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][3] != null && (thisYear - arr[i][3] > 0)) {
                tampung = {
                    firstName : arr[i][0],
                    lastName : arr[i][1],
                    gender : arr[i][2],
                    age : thisYear - arr[i][3]
                }
            } else {
                tampung = {
                    firstName : arr[i][0],
                    lastName : arr[i][1],
                    gender : arr[i][2],
                    age : "Invalid Birth Year"
                }

            }
            object.push(tampung);
        }

        for (var i = 0; i < arr.length; i++) {
            console.log((i+1) + ". " + object[i].firstName + " " + object[i].lastName)
            console.log(object[i])
        }
        console.log(" ");
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

//SOAL 2
console.log("\nSoal No. 2 (Shopping Time)");  

function shoppingTime(memberId, money) {
  // you can only write your code here!
  var dataBarang = [
    {barang : "Sepatu Stacattu",harga : 1500000},{barang : "Baju Zoro",harga : 500000},
    {barang : "Baju H&N",harga : 250000},{barang : "Sweater Uniklooh",harga : 175000},
    {barang : "Casing Handphone",harga : 50000}
  ]
  var shopping = [];
  var cart = [];
  var pesan = "";
  if (memberId == null || memberId == '') {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000){
    return "Mohon maaf, uang tidak cukup";
  } else {
    shopping.memberId = memberId;
    shopping.money = money;
    for (var i = 0; i < dataBarang.length; i++) {
        if (dataBarang[i].harga <= money) {
            cart.push(dataBarang[i].barang);
            money-=dataBarang[i].harga;
        }
    }
    shopping.listPurchased = cart;
    shopping.changeMoney = money;
    return shopping;
  }
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//SOAL 3
console.log("\nSoal No. 3 (Naik Angkot)");
function naikAngkot(arrPenumpang) {
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  var perjalanan = [];
  if (arrPenumpang[0] == null) {
    return perjalanan;
  } else {
    for (var i = 0; i < arrPenumpang.length; i++) {
        var jarak = 0;
        var naik = false;
        for (var j = 0; j < rute.length; j++) {
            if (arrPenumpang[i][1] == rute[j]) {
                naik = true;
            } 
            if (naik) {
                if (arrPenumpang[i][2] == rute[j]) {
                    break;
                }
                jarak+=1;
            }
        }
        if (!naik) {
            console.log("Rute tidak sesuai");
        } else {
            var harga = jarak * 2000;
        }

        var tampung = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: harga
          }
        perjalanan.push(tampung);
      }
      return perjalanan;
  }
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]