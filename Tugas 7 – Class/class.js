//1. Animal Class
console.log("1. Animal Class");

//Release 0
console.log("Release 0");
class Animal {
    constructor(name,legs = 4,cold_blooded = false) {
        this.name = name
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//Release 1
console.log("\nRelease 1");
// Code class Ape dan class Frog di sini
class Ape extends Animal{
	constructor(name) {
        super(name,2)
    }

    yell(){
    	console.log("Auoo");
    }
}

class Frog extends Animal{
	constructor(name) {
        super(name,4);
    }

    jump(){
    	console.log("hop hop");
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//2. Function to Class
console.log("\n2. Function to Class");

class Clock {
    // Code di sini
    constructor({ template }) {
        this._timer
        this._template = template
    }

    render() {
	    var date = new Date();

	    var hours = date.getHours();
	    if (hours < 10) hours = '0' + hours;

	    var mins = date.getMinutes();
	    if (mins < 10) mins = '0' + mins;

	    var secs = date.getSeconds();
	    if (secs < 10) secs = '0' + secs;

	    var output = this._template
	      .replace('h', hours)
	      .replace('m', mins)
	      .replace('s', secs);

	    console.log(output);
  	}

  	stop() {
    	clearInterval(_timer);
	};

  	start() {
	    this.render();
	    this._timer = setInterval(this.render.bind(this), 1000);
  	};
}

var clock = new Clock({template: 'h:m:s'});
clock.start();